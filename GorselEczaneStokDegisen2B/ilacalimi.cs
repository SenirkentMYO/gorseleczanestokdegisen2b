﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GorselEczaneStokDegisen2B
{
    public partial class ilacalimi : Form
    {
        public ilacalimi()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Form1 m = new Form1();
            this.Hide();

            m.ShowDialog();



        }

        private void ilacalimi_Load(object sender, EventArgs e)
        {
            ECZANEEntities6 database = new ECZANEEntities6();
            List<İlacAlım> alım = database.İlacAlım.ToList();
            dataGridView1.DataSource = alım;

        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            Form1 m = new Form1();
            this.Hide();

            m.ShowDialog();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ECZANEEntities6 database = new ECZANEEntities6();
            List<İlacAlım> alim = database.İlacAlım.Where(a => a.İlacAdi.Contains(textBox5.Text)).ToList();
            dataGridView1.DataSource = alim;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            ECZANEEntities6 database = new ECZANEEntities6();
            List<İlacAlım> alim = database.İlacAlım.ToList();
            dataGridView1.DataSource = alim;
            textBox1.Clear();
            textBox2.Clear();
            textBox3.Clear();
            textBox4.Clear();
            textBox5.Clear();
            textBox6.Clear();
            textBox7.Clear();




        }

        private void button3_Click(object sender, EventArgs e)
        {

            SqlConnection baglan = new SqlConnection(@"Data source=MEHMETŞIRIN\SQLEXPRESS;initial catalog=ECZANE; integrated security=true");
            SqlCommand cmd = new SqlCommand("insert into İlacAlım(ID,İlacAdi,FirmaAdi,DepoAdi,Fiyat,Stok)values(@ID,@İlacAdi,@FirmaAdi,@DepoAdi,@Fiyat,@Stok)", baglan);
            baglan.Open();
            cmd.Parameters.AddWithValue("@ID", textBox1.Text);
            cmd.Parameters.AddWithValue("@İlacAdi", textBox2.Text);
            cmd.Parameters.AddWithValue("@FirmaAdi", textBox3.Text);
            cmd.Parameters.AddWithValue("@DepoAdi", textBox4.Text);
            cmd.Parameters.AddWithValue("@Fiyat", textBox6.Text);
            cmd.Parameters.AddWithValue("@Stok", textBox5.Text);

            cmd.ExecuteNonQuery();
            baglan.Close();
            MessageBox.Show("Başarıyla Eklendi", "Bilgi");
            ECZANEEntities6 database = new ECZANEEntities6();
            List<İlacAlım> alim = database.İlacAlım.ToList();
            dataGridView1.DataSource = alim;



     
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            textBox1.Text = dataGridView1.CurrentRow.Cells[0].Value.ToString();
            textBox2.Text = dataGridView1.CurrentRow.Cells[1].Value.ToString();
            textBox3.Text = dataGridView1.CurrentRow.Cells[2].Value.ToString();
            textBox4.Text = dataGridView1.CurrentRow.Cells[3].Value.ToString();
            textBox5.Text = dataGridView1.CurrentRow.Cells[4].Value.ToString();
           // textBox6.Text = dataGridView1.CurrentRow.Cells[5].Value.ToString();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
            