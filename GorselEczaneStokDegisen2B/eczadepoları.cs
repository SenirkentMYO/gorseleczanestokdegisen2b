﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GorselEczaneStokDegisen2B
{
    public partial class eczadepoları : Form
    {
        public eczadepoları()
        {
            InitializeComponent();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Form1 a = new Form1();
            this.Hide();

            a.ShowDialog();
        }

        private void button5_Click_1(object sender, EventArgs e)
        {
            Form1 v = new Form1();
            this.Hide();
            v.ShowDialog();
        }

        private void eczadepoları_Load(object sender, EventArgs e)
        {
            ECZANEEntities6 database = new ECZANEEntities6();
            List<İlacAlım> depo = database.İlacAlım.ToList();
            dataGridView1.DataSource = depo;
        }

        private void button6_Click(object sender, EventArgs e)
        {
            ECZANEEntities6 database = new ECZANEEntities6();
            List<İlacAlım> depo = database.İlacAlım.ToList();
            dataGridView1.DataSource = depo;
            textBox1.Clear();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            textBox1.Text = dataGridView1.CurrentRow.Cells[0].Value.ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ECZANEEntities6 database = new ECZANEEntities6();
            List<İlacAlım> depo = database.İlacAlım.Where(a => a.İlacAdi.Contains(textBox1.Text)).ToList();
            dataGridView1.DataSource = depo;
        }
    }
}
