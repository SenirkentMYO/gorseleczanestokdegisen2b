﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GorselEczaneStokDegisen2B
{
    public partial class İlacsatisi : Form
    {
        public İlacsatisi()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Form1 i = new Form1();
            this.Hide();

            i.ShowDialog();
            
        }

        private void button4_Click(object sender, EventArgs e)
        {
            ECZANEEntities6 database = new ECZANEEntities6();
            List<İlacAlım> satış = database.İlacAlım.ToList();
            dataGridView1.DataSource = satış;
            textBox1.Clear();
            textBox2.Clear();
            textBox3.Clear();
            textBox4.Clear();
            textBox5.Clear();
            textBox6.Clear();
            textBox7.Clear();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ECZANEEntities6 database = new ECZANEEntities6();
            List<İlacAlım> satıs = database.İlacAlım.Where(a => a.İlacAdi.Contains(textBox5.Text)).ToList();
            dataGridView1.DataSource = satıs;
        }

        private void İlacsatisi_Load(object sender, EventArgs e)
        {
            ECZANEEntities6 database = new ECZANEEntities6();
            List<İlacAlım> satış = database.İlacAlım.ToList();
            dataGridView1.DataSource = satış;
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            textBox1.Text = dataGridView1.CurrentRow.Cells[0].Value.ToString();
            textBox2.Text = dataGridView1.CurrentRow.Cells[1].Value.ToString();
            textBox3.Text = dataGridView1.CurrentRow.Cells[2].Value.ToString();
            textBox4.Text = dataGridView1.CurrentRow.Cells[3].Value.ToString();
            textBox7.Text = dataGridView1.CurrentRow.Cells[4].Value.ToString();
           // textBox6.Text = dataGridView1.CurrentRow.Cells[5].Value.ToString();
        }
    }
}
